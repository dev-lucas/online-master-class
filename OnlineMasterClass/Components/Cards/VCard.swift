//
//  VCard.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 27/07/23.
//

import SwiftUI
import CoreMotion

struct VCard: View {
    @EnvironmentObject var model: Model
    @State private var courseImageOffset: CGSize = .zero
    let motionManager = CMMotionManager()

    var course: Course

    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            Text(course.caption)
                .customFont(.subheadline2)
                .foregroundColor(.white)
                .padding(.horizontal, 13)
                .padding(.vertical, 6)
                .background(
                    Capsule()
                        .fill(.white.opacity(0.3))
                        .shadow(color: course.secondColor.opacity(0.2), radius: 5, x: 0, y: 5)
                )
            
            Text(course.title)
                .lineLimit(1)
                .customFont(.cardTitle)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            Text(course.subtitle)
                .lineLimit(1)
                .customFont(.title)
                .frame(maxWidth: .infinity, alignment: .leading)
                .offset(y: -22)
            
            Spacer()
        }
        .foregroundColor(.white)
        .padding(25)
        .frame(width: 240, height: 345)
        .background(.linearGradient(colors: [course.color, course.secondColor], startPoint: .top, endPoint: .bottom))
        .overlay(
            course.image
                .resizable()
                .scaledToFit()
                .frame(width: 280)
                .frame(maxHeight: .infinity, alignment: .bottom)
                .offset(x: 25)
                .offset(courseImageOffset)
        )
        .mask(
            RoundedRectangle(cornerRadius: 30, style: .continuous)
        )
        .shadow(color: Color(hex: "24214C").opacity(0.7), radius: 10, x: 0, y: 5)
        .shadow(color: Color(hex: "24214C").opacity(0.6), radius: 2, x: 0, y: 1)
        .overlay(
            RoundedRectangle(cornerRadius: 30, style: .continuous)
//                .stroke(.white.opacity(0.2), lineWidth: 0.7)
                .stroke(.white.opacity(0.2), lineWidth: 0.7)
        )
        .scaleEffect(model.showDetail ? 0.9 : 1)
        .onTapGesture {
            withAnimation(.spring(response: 0.5, dampingFraction: 0.7)) {
                Settings.Sidebar.showButton = false
                model.showDetail = true
                model.selectedCourse = course.index
            }
        }
        .onAppear {
            motionManager.startDeviceMotionUpdates(to: .main) { motion, error in
                guard let motion = motion else { return }
                let xAngle = motion.attitude.roll
                let yAngle = motion.attitude.pitch
                
                courseImageOffset = CGSize(width: xAngle * 7, height: yAngle * 7)
            }
        }
        .onDisappear {
            motionManager.stopDeviceMotionUpdates()
        }
    }
}

struct VCard_Previews: PreviewProvider {
    static var previews: some View {
        VCard(course: courses[0])
            .environmentObject(Model())
    }
}
