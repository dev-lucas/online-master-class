//
//  HCardSection.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 14/09/23.
//

import SwiftUI

struct HCardSection: View {
    var course: SectionFreeCourse
    var isHighlight: Bool
    
    var body: some View {
        HStack(spacing: 15) {
            RoundedRectangle(cornerRadius: 22, style: .circular)
                .fill(course.color)
                .frame(width: 76, height: 76)
                .offset(x: 0, y: 0)
                .shadow(color: Color.background, radius: 10, x: 5, y: 5)
                .overlay(
                    course.image
                        .resizable()
                        .scaledToFit()
                        .frame(width: 82)
                        .shadow(color: course.color.opacity(0.2), radius: 3, x: 0, y: 3)
                        .overlay(
                            PlayButton()
                                .scaleEffect(0.8)
                        )
                )
            
            VStack(alignment: .leading, spacing: 5) {
                Text(course.title)
                    .customFont(.title4)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                HStack(spacing: 18) {
                    Text(course.minutes)
                        .customFont(.caption)
                        .foregroundColor(Color("Secondary Text"))
                    
                    if course.isFree {
                        Text("Free")
                            .customFont(.caption2)
                            .background(
                                RoundedRectangle(cornerRadius: 18)
                                    .foregroundColor(Color("AccentColor"))
                                    .frame(width: 40, height: 18)
                            )
                    }
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: 76)
        .background(
            Color(isHighlight ? "Background 2" : "Background")
                .mask(RoundedRectangle(cornerRadius: 25, style: .continuous))
        )
        .foregroundColor(.white)
    }
}

struct HCardSection_Previews: PreviewProvider {
    static var previews: some View {
        HCardSection(course: sectionFreeCourses[3], isHighlight: true)
            .padding(.horizontal, 30)
    }
}
