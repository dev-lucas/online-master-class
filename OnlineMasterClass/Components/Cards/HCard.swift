//
//  HCard.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 27/07/23.
//

import SwiftUI

struct HCard: View {
    var course: FreeCourse
    
    var body: some View {
        HStack(spacing: 0) {
            RoundedRectangle(cornerRadius: 20, style: .continuous)
                .fill(course.color)
                .frame(width: 130, height: 95)
                .offset(x: -13, y: -14)
                .shadow(color: Color.background, radius: 10, x: 5, y: 5)
                .overlay(
                    course.image
                        .resizable()
                        .scaledToFit()
                        .frame(width: 110)
                        .offset(x: -13, y: -22)
                        .shadow(color: course.color.opacity(0.2), radius: 3, x: 0, y: 3)
                )
            
            VStack(alignment: .leading, spacing: 2) {
                Text(course.title)
                    .customFont(.title4)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                Text(course.minutes)
                    .customFont(.caption)
                    .foregroundColor(Color("Secondary Text"))
                
             
                RatingStars(rating: course.rating)
                    .frame(width: 90, height: 17)
                    .offset(x: -4)
            }
        }
        .padding(30)
        .frame(maxWidth: .infinity, maxHeight: 100)
        .background(
            Color("Background 2")
                .mask(RoundedRectangle(cornerRadius: 30, style: .continuous))
                .overlay(
                    Circle()
                        .fill(Color.accentColor)
                        .frame(width: 42, height: 42)
                        .frame(maxWidth: .infinity, alignment: .trailing)
                        .offset(x: 21)
                        .shadow(color: Color.background, radius: 15, x: 0, y: 0)
                        .overlay(
                            Image(systemName: "play.fill")
                                .resizable()
                                .frame(width: 6, height: 10)
                                .frame(maxWidth: .infinity, alignment: .trailing)
                                .offset(x: 4)
                        )
                )
        )
        .padding(.trailing, 20)
        .foregroundColor(.white)
    }
}

struct HCard_Previews: PreviewProvider {
    static var previews: some View {
        HCard(course: freeCourses[1])
    }
}
