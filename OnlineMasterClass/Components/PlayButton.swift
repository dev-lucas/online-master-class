//
//  PlayButton.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 25/09/23.
//

import SwiftUI

struct PlayButton: View {
    @State private var isTouch = false
    
    var body: some View {
        Image(systemName: isTouch ? "play.fill" : "play.fill")
            .font(.system(size: 18, weight: .bold))
            .foregroundColor(.white)
            .padding(.vertical, 14)
            .padding(.horizontal, 14)
            .background(
                Circle()
                    .fill(.ultraThinMaterial)
                    .opacity(0.7)
            )
            .animation(.spring(), value: isTouch)
            .onTapGesture {
                withAnimation {
                    isTouch.toggle()
                }
            }
            .environment(\.colorScheme, .dark)
    }
}

struct PlayButton_Previews: PreviewProvider {
    static var previews: some View {
        PlayButton()
    }
}
