//
//  MenuRow.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 18/09/23.
//

import SwiftUI

struct MenuRow: View {
    @AppStorage("selectedTab") var menus: SelectedMenu = .home
    @Binding var selectedMenu: SelectedMenu
    var item: MenuItem
    
    var body: some View {
        HStack(spacing: 14) {
            item.icon.view()
                .frame(width: 32, height: 32)
                .opacity(0.6)
            
            Text(item.text)
                .customFont(.headline)
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(12)
        .background(
            RoundedRectangle(cornerRadius: 10, style: .continuous)
                .fill(Color("Primary"))
                .frame(maxWidth: selectedMenu == item.menu ? .infinity : 0)
                .frame(maxWidth: .infinity, alignment: .leading)//For animation
        )
        .background(Color.background2)
        .onTapGesture {
            item.icon.setInput("active", value: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                item.icon.setInput("active", value: false)
            }
            withAnimation(.timingCurve(0.2, 0.8, 0.2, 1)) {
                selectedMenu = item.menu
                menus = selectedMenu
            }
        }
        .onAppear {
            menus = selectedMenu
        }
    }
}

struct MenuRow_Previews: PreviewProvider {
    static var previews: some View {
        MenuRow(selectedMenu: .constant(.home), item: menuItems[0])
    }
}
