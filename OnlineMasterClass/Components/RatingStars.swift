//
//  RatingStars.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 18/09/23.
//

import SwiftUI
import RiveRuntime

struct RatingStars: View {
    var rating: Double
    
    var body: some View {
        let ratingStars = RiveViewModel(fileName: "rating_animation", autoPlay: false)
        let delayIncrement: TimeInterval = 0.15
        
        VStack {
            ratingStars.view()
        }
        .onAppear {
            var delay: TimeInterval = 0.5
            
            for i in 1...Int(rating) {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    ratingStars.setInput("rating", value: Double(i))
                }
                delay += delayIncrement
            }
        }
        .allowsHitTesting(false)
    }
}

struct RatingStars_Previews: PreviewProvider {
    static var previews: some View {
        RatingStars(rating: 5)
    }
}
