//
//  LikeButton.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 14/09/23.
//

import SwiftUI

struct LikeButton: View {
    @State private var isLiked = false
    
    var body: some View {
        Image(systemName: isLiked ? "hand.thumbsup.fill" : "hand.thumbsup")
            .font(.system(size: 18, weight: .bold))
            .foregroundColor(.white)
            .padding(.vertical, 14)
            .padding(.horizontal, 14)
            .background(Color.background2, in: RoundedRectangle(cornerRadius: 18))
            .rotationEffect(Angle.degrees(isLiked ? 360 : 0))
            .scaleEffect(isLiked ? 1.2 : 1.0)
            .animation(.spring())
            .onTapGesture {
                withAnimation {
                    isLiked.toggle()
                }
            }
    }
}

struct LikeButton_Previews: PreviewProvider {
    static var previews: some View {
        LikeButton()
    }
}
