//
//  CloseButton.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 03/08/23.
//

import SwiftUI

struct CloseButton: View {
    var body: some View {
        Image(systemName: "chevron.left")
            .font(.system(size: 17, weight: .bold))
            .foregroundColor(.white)
            .padding(.vertical, 14)
            .padding(.horizontal, 16)
            .background(.black.opacity(0.2), in: RoundedRectangle(cornerRadius: 14))
    }
}

struct CloseButton_Previews: PreviewProvider {
    static var previews: some View {
        CloseButton()
            .background(.orange)
    }
}
