//
//  HAvatars.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 14/09/23.
//

import SwiftUI

struct HAvatars: View {
    @State private var shouldAnimate = false

    var body: some View {
        HStack {
            ForEach(Array([1, 2, 3, 4].shuffled().enumerated()), id: \.offset) { index, number in
                ZStack {
                    Image("avatar-\(number)")
                        .resizable()
                        .frame(width: 32, height: 32)
                        .mask(Circle())
                        .zIndex(Double(-index))
                        .overlay(
                            Circle()
                                .stroke(Color.background, lineWidth: 2)
                        )
                        .rotation3DEffect(.degrees(shouldAnimate ? 0 : 180),
                            axis: (x: 0, y: 1, z: 0),
                            anchor: .center,
                            anchorZ: 0,
                            perspective: 1.0
                        )
                        .offset(x: CGFloat(index * -5))
                }
            }
            .padding(.trailing, -15)
        }
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                withAnimation(.spring(response: 0.6, dampingFraction: 0.6)) {
                    shouldAnimate = true
                }
            }
        }
    }
}

struct HAvatars_Previews: PreviewProvider {
    static var previews: some View {
        HAvatars()
    }
}
