//
//  Colors.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 18/09/23.
//

import SwiftUI

extension Color {
    static let primary = Color("Primary")
    static let background2 = Color("Background 2")
    static let background = Color("Background")
    static let secondaryText = Color("Secondary Text")
    static let shadow = Color("Shadow")
}
