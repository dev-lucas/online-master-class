//
//  View+Extensions.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 25/09/23.
//

import SwiftUI

enum Directions {
    case top
    case bottom
    case all
}

extension View {
    @ViewBuilder
    func elasticScroll(scrollRect: CGRect, screenSize: CGSize, directions: Directions) -> some View {
        self.modifier(ElasticScrollHelper(scrollRect: scrollRect, screenSize: screenSize, directions: directions))
    }
}

fileprivate struct ElasticScrollHelper: ViewModifier {
    var scrollRect: CGRect
    var screenSize: CGSize
    var directions: Directions
    @State private var viewRect: CGRect = .zero
    func body(content: Content) -> some View {
        let progress = scrollRect.minY / scrollRect.maxY
        let elasticOffset = (progress * viewRect.minY)// * 2
        let bottomProgress = max(1 - (scrollRect.maxY / screenSize.height), 0)
        let bottomElasticOffset = (viewRect.maxY * bottomProgress)
        content
            .offset(y: (directions == .top || directions == .all) ? (scrollRect.minY > 0 ? elasticOffset : 0) : 0)
            .offset(y: (directions == .top || directions == .all) ? (scrollRect.minY > 0 ? -(progress * scrollRect.minY) : 0) : 0)
            .offset(y: (directions == .bottom || directions == .all) ? (scrollRect.maxY < screenSize.height ? bottomElasticOffset : 0) : 0)
            .offsetExtractor(coordinateSpace: "ScrollView") {
                viewRect = $0
            }
    }
}

struct View_Extensions_Previews: PreviewProvider {
    static var previews: some View {
        CourseView(course: .constant(courses[1]))
            .environmentObject(Model())
    }
}
