//
//  OffsetHelper.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 25/09/23.
//

import SwiftUI

struct OffsetHelper: PreferenceKey {
    static var defaultValue: CGRect = .zero
    static func reduce(value: inout Value, nextValue: () -> Value) {
        value = nextValue()
    }
}

extension View {
    @ViewBuilder
    func offsetExtractor(coordinateSpace: String, completion: @escaping (CGRect) -> ()) -> some View {
        self.overlay {
            GeometryReader {
                let rect = $0.frame(in: .named(coordinateSpace))
                Color.clear
                    .preference(key: OffsetHelper.self, value: rect)
                    .onPreferenceChange(OffsetHelper.self, perform: completion)
            }
        }
    }
}

