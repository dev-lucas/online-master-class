//
//  Course.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 27/07/23.
//

import SwiftUI

struct Course: Identifiable {
    var id = UUID()
    var index: Int
    var title: String
    var subtitle: String
    var caption: String
    var color: Color
    var secondColor: Color
    var image: Image
}

var courses = [
    Course(index: 1, title: "UI design", subtitle: "for beginner", caption: "Featured!", color: Color(hex: "9589E9"), secondColor: Color(hex: "504AAA"), image: Image("CardSample 1")),
    Course(index: 2, title: "Graphic Design", subtitle: "Master", caption: "Discount!", color: Color(hex: "FFC154"), secondColor: Color(hex: "DA2152"), image: Image("CardSample 2")),
]
