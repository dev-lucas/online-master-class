//
//  FreeCourse.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 27/07/23.
//

import SwiftUI

struct FreeCourse: Identifiable {
    var id = UUID()
    var title: String
    var minutes: String
    var rating: Double
    var isFree: Bool
    var color: Color
    var image: Image
}

var freeCourses = [
    FreeCourse(title: "3D Illustrator Pro", minutes: "24 minute", rating: 5.0, isFree: false, color: Color(hex: "7DD1FF"), image: Image("CardSample 3")),
    FreeCourse(title: "Drawing Master", minutes: "38 minute", rating: 4.0, isFree: true, color: Color(hex: "FFC65D"), image: Image("CardSample 4")),
    FreeCourse(title: "Board Check", minutes: "53 minute", rating: 2.0, isFree: false, color: Color(hex: "D3729D"), image: Image("CardSample 5")),
    FreeCourse(title: "World Animations", minutes: "26 minute", rating: 5.0, isFree: false, color: Color(hex: "54FFAD"), image: Image("CardSample 6")),
]

struct SectionFreeCourse: Identifiable {
    var id = UUID()
    var title: String
    var minutes: String
    var isFree: Bool
    var color: Color
    var image: Image
}

var sectionFreeCourses = [
    SectionFreeCourse(title: "Introduction to Graphic design", minutes: "12 minute", isFree: true, color: Color(hex: "3CDCBA"), image: Image("CardSample 7")),
    SectionFreeCourse(title: "Fundamentals of Design", minutes: "26 minute", isFree: false, color: Color(hex: "D9539A"), image: Image("CardSample 8")),
    SectionFreeCourse(title: "Layout and Composition", minutes: "53 minute", isFree: false, color: Color(hex: "7568D4"), image: Image("CardSample 9")),
    SectionFreeCourse(title: "Make 3d Objects", minutes: "26 minute", isFree: false, color: Color(hex: "4169E1"), image: Image("CardSample 10")),
]

var searchCourses = [
    SectionFreeCourse(title: "Introduction to Graphic design", minutes: "12 minute", isFree: true, color: Color(hex: "3CDCBA"), image: Image("CardSample 7")),
    SectionFreeCourse(title: "Fundamentals of Design", minutes: "26 minute", isFree: false, color: Color(hex: "D9539A"), image: Image("CardSample 8")),
    SectionFreeCourse(title: "Layout and Composition", minutes: "53 minute", isFree: false, color: Color(hex: "7568D4"), image: Image("CardSample 9")),
    SectionFreeCourse(title: "Make 3d Objects", minutes: "26 minute", isFree: false, color: Color(hex: "4169E1"), image: Image("CardSample 10")),
    SectionFreeCourse(title: "3D Illustrator Pro", minutes: "24 minute", isFree: false, color: Color(hex: "7DD1FF"), image: Image("CardSample 3")),
    SectionFreeCourse(title: "Drawing Master", minutes: "38 minute", isFree: true, color: Color(hex: "FFC65D"), image: Image("CardSample 4")),
    SectionFreeCourse(title: "Board Check", minutes: "53 minute", isFree: false, color: Color(hex: "D3729D"), image: Image("CardSample 5")),
    SectionFreeCourse(title: "World Animations", minutes: "26 minute", isFree: false, color: Color(hex: "54FFAD"), image: Image("CardSample 6")),
    SectionFreeCourse(title: "Make 3d Objects", minutes: "26 minute", isFree: false, color: Color(hex: "4169E1"), image: Image("CardSample 10")),
    SectionFreeCourse(title: "3D Illustrator Pro", minutes: "24 minute", isFree: false, color: Color(hex: "7DD1FF"), image: Image("CardSample 3")),
    SectionFreeCourse(title: "Drawing Master", minutes: "38 minute", isFree: true, color: Color(hex: "FFC65D"), image: Image("CardSample 4")),
    SectionFreeCourse(title: "Introduction to Graphic design", minutes: "12 minute", isFree: true, color: Color(hex: "3CDCBA"), image: Image("CardSample 7")),
    SectionFreeCourse(title: "Fundamentals of Design", minutes: "26 minute", isFree: false, color: Color(hex: "D9539A"), image: Image("CardSample 8")),
    SectionFreeCourse(title: "Layout and Composition", minutes: "53 minute", isFree: false, color: Color(hex: "7568D4"), image: Image("CardSample 9")),
    SectionFreeCourse(title: "3D Illustrator Pro", minutes: "24 minute", isFree: false, color: Color(hex: "7DD1FF"), image: Image("CardSample 3")),
    SectionFreeCourse(title: "Drawing Master", minutes: "38 minute", isFree: true, color: Color(hex: "FFC65D"), image: Image("CardSample 4")),
]
