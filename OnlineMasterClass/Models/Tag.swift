//
//  Tag.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 28/09/23.
//

import SwiftUI

struct Tag: Identifiable, Hashable {
    var id: UUID = .init()
    var value: String
    var isInitial: Bool = false
}
