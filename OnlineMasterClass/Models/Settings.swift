//
//  Settings.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 21/09/23.
//

import SwiftUI
import Combine

class Settings: ObservableObject {
    @Published var showSidebarButton: Bool = true

    static var sidebar: Sidebar {
        return Sidebar(settings: shared)
    }

    private static var shared = Settings()

    private init() {}
}

extension Settings {
    struct Sidebar {
        @ObservedObject var settings: Settings
        
        init(settings: Settings) {
            self.settings = settings
        }

        var showButton: Bool {
            get {
                settings.showSidebarButton
            }
            set {
                settings.showSidebarButton = newValue
            }
        }

        static var showButton: Bool {
            get {
                return Settings.shared.showSidebarButton
            }
            set {
                Settings.shared.showSidebarButton = newValue
            }
        }
    }
}
