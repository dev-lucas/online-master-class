//
//  Model.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 28/07/23.
//

import SwiftUI
import Combine

class Model: ObservableObject {
    
    // Course Detail View
    @Published var showDetail: Bool = false
    @Published var selectedCourse: Int = 0
}
