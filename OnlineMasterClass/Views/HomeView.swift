//
//  HomeView.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 27/07/23.
//

import SwiftUI
import RiveRuntime

struct HomeView: View {
    @EnvironmentObject var model: Model
    @State private var showCards = false
    @State var isOpen: Bool = false
    @State var show: Bool = false
    @State var showMenuSidebar: Bool = true
    @State private var fCourses: [FreeCourse] = freeCourses
    @State private var isLoading = false
    @AppStorage("selectedTab") var selectedMenu: SelectedMenu = .search
    
    let sidebarButton = RiveViewModel(fileName: "menu_button", stateMachineName: "State Machine", autoPlay: false)

    func cardAnimation(for index: Int) -> Animation {
        return Animation.spring(response: 0.5, dampingFraction: 0.8)
            .delay(Double(index) * 0.2)
    }

    //MARK: - Main Body

    var body: some View {
        showView()
            .safeAreaInset(edge: .top) {
                Color.clear.frame(height: 60)//Adiciona um espaco extra na parte inferior
            }
            .mask(
                RoundedRectangle(cornerRadius: 30, style: .continuous)
            )
            .rotation3DEffect(.degrees(isOpen ? -30 : 0), axis: (x: 0, y: -1, z: 0))
            .offset(x: isOpen ? -265 : 0)
            .scaleEffect(isOpen ? 0.9 : 1)
            .scaleEffect(show ? 0.92 : 1)
            .ignoresSafeArea(.all)
            .allowsHitTesting(!isOpen)
        
        SideMenu()
            .opacity(isOpen ? 1 : 0)
            .offset(x: isOpen ? 110 : 400)
            .rotation3DEffect(.degrees(isOpen ? 0 : 30), axis: (x: 0, y: -1, z: 0))
        
        sidebarButton.view()
            .frame(width: 46, height: 46)
            .mask(Circle())
            .shadow(color: Color.shadow.opacity(0.2), radius: 5, x: 0, y: 5)
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
            .offset(x: 320, y: Settings.sidebar.showButton ? 12 : -1000)
            .onTapGesture {
                sidebarButton.setInput("isOpen", value: isOpen)
                withAnimation(.spring(response: 0.5, dampingFraction: 0.7)) {
                    isOpen.toggle()
                }
            }
    }

    //MARK: - Home Settings

    var home: some View {
        ZStack {
            GeometryReader { geometry in
                Color.background
                    .ignoresSafeArea()
                
                //MARK: Top left shadow
                Circle()
                    .fill(Color.white.opacity(0.9))
                    .frame(width: 500, height: 500)
                    .position(x: (geometry.size.width / 2) - 370, y: (geometry.safeAreaInsets.top) - 270)
                    .shadow(color: .white, radius: 400, x: 0, y: 0)
                    .ignoresSafeArea()
                    .zIndex(1)
                
                //MARK: Background for CourseDetail open
                Color.background
                    .ignoresSafeArea()
                    .opacity(model.showDetail ? 1 : 0)
                    .zIndex(model.showDetail ? 1 : 0)

                if model.showDetail {
                    detail
                }

                ScrollView(.vertical) {
                    content
                }
            }
            .foregroundColor(.white)
        }
    }

    //MARK: - Home Content Data

    var content: some View {
        VStack(alignment: .leading, spacing: 0) {
            Text("Online")
                .customFont(.largeTitleBold)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.horizontal, 20)
            
            Text("Master Class")
                .customFont(.largeTitle)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.horizontal, 20)
                .offset(y: -12)
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 20) {
                    ForEach(courses) { course in
                        VCard(course: course)
                    }
                }
                .padding(.horizontal, 20)
                .padding(.top, 10)
                .padding(.bottom, 20)
            }
            
            Text("Free Online Class")
                .customFont(.title2)
                .padding(.horizontal, 20)
                .padding(.top, 20)
            
            Text("From over 80 Lecturers")
                .customFont(.caption)
                .padding(.horizontal, 20)
                .padding(.top, 5)
                .foregroundColor(Color.secondaryText)
        
            VStack(spacing: 30) {
                ForEach(freeCourses.indices, id: \.self) { index in
                    HCard(course: freeCourses[index])
                        .offset(y: showCards ? 0 : (index == 0 ? 1.0 : CGFloat(index)) * 1000)
                        .animation(.spring(response: 0.5, dampingFraction: 0.8).delay(Double(index) * 0.2), value: showCards)
                }
            }
            .onAppear {
                withAnimation {
                    showCards = true
                }
            }
            .padding(.vertical, 15)
            .padding(20)
        }
    }

    //MARK: - Course Details
    
    var detail: some View {
        ForEach(courses) { course in
            if course.index == model.selectedCourse {
                CourseView(course: .constant(course))
                    .zIndex(2)
            }
        }
    }
    
    func showView() -> AnyView {
        switch selectedMenu {
        case .home:
            return AnyView(home)
        case .search:
            return AnyView(SearchView())
        case .favorites:
            return exampleViews
        case .help:
            return exampleViews
        case .history:
            return exampleViews
        case .notifications:
            return exampleViews
        case .darkmode:
            return exampleViews
        }
    }
    
    var exampleViews: AnyView {
        AnyView(
            Color.background
                .ignoresSafeArea()
                .overlay(
                    Text("\(selectedMenu.rawValue)")
                )
        )
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
            .environmentObject(Model())
    }
}
