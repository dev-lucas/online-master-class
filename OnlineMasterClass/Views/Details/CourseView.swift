//
//  CourseView.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 28/07/23.
//

import SwiftUI

struct CourseView: View {
    @Binding var course: Course
    @State private var yOffset: CGFloat = -1000
    @State private var showCards = false
    @State private var scrollRect: CGRect = .zero
    @EnvironmentObject var model: Model

    var body: some View {
        GeometryReader {
            let size = $0.size

            Color.background
                .ignoresSafeArea()

                ScrollView(.vertical, showsIndicators: false) {
                    cover
                        .shadow(color: .black.opacity(0.3), radius: 10, x: 5, y: 5)
                        .offset(y: yOffset)
                        .rotation3DEffect(.degrees(yOffset / -10), axis: (x: 1, y: 0, z: 0))
                    
                    VStack(alignment: .leading, spacing: 10) {
                        RatingStars(rating: 4)
                            .frame(width: 90, height: 17)
                            .offset(x: -5)
                        
                        Text("\(course.title) \(course.subtitle)")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .customFont(.title)
                        
                        HStack(alignment: .center, spacing: 10) {
                            HStack(spacing: 10) {
                                HAvatars()
                                
                                Text("Mike Patt, Kim Young and\n8 friends Like this class.")
                                    .frame(maxWidth: .infinity)
                                    .customFont(.caption)
                                    .foregroundColor(Color("Secondary Text"))
                            }
                            Spacer()
                            LikeButton()
                        }
                        .padding(.top, 10)

                        VStack(spacing: 20) {
                            ForEach(Array(sectionFreeCourses.enumerated()), id: \.1.id) { index, course in
                                HCardSection(course: course, isHighlight: true)
                                    .offset(x: showCards ? 0 : (index == 0 ? 1.0 : CGFloat(index)) * (index % 2 == 0 ? 1000 : -1000), y: 0)
                                    .animation(.easeOut(duration: 0.5).delay(Double(index) * 0.2), value: showCards)
                                    .elasticScroll(scrollRect: scrollRect, screenSize: size, directions: .bottom)
                            }
                        }
                        .padding(.vertical, 20)
                        .offsetExtractor(coordinateSpace: "ScrollView") {
                            scrollRect = $0
                        }
                        .onAppear {
                            withAnimation {
                                showCards = true
                            }
                        }
                    }
                    .offset(y: yOffset * -1)
                    .padding(.top, 355)
                    .padding(.horizontal, 26)
                }
                .coordinateSpace(name: "ScrollView")

            Button {
                withAnimation {
                    yOffset = -1000
                    Settings.Sidebar.showButton = true
                    model.showDetail = false
                    model.selectedCourse = 0
                }
            } label: {
                CloseButton()
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
            .padding(.horizontal, 25)
        }
        .onAppear {
            withAnimation(.spring(response: 0.6, dampingFraction: 0.9)) {
                yOffset = 0
            }
        }
        .foregroundColor(.white)
    }
    
    var cover: some View {
        GeometryReader { proxy in
            let scrollY = proxy.frame(in: .named("scroll")).minY
            
            VStack {
            }
            .frame(maxWidth: .infinity)
            .frame(height: scrollY > 0 ? 350 + scrollY : 350)
            .background(
                course.image
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .scaleEffect(1.2 + scrollY/2000)
                    .offset(y: scrollY > 0 ? -scrollY : 0)
                    .offset(x: 50, y: -10)
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom)
            )
            .background(
                LinearGradient(
                    colors: [course.color, course.secondColor], startPoint: .top, endPoint: .bottom
                )
                .offset(y: scrollY > 0 ? -scrollY : 0)
                .scaleEffect(scrollY > 0 ? scrollY / 500 + 1 : 1)
            )
            .mask(
                RoundedRectangle(cornerRadius: 30)
                    .offset(y: scrollY > 0 ? -scrollY : 0)
            )
        }
    }
}

struct CourseView_Previews: PreviewProvider {
    static var previews: some View {
        CourseView(course: .constant(courses[1]))
            .environmentObject(Model())
    }
}
