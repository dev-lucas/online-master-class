//
//  SearchView.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 26/09/23.
//

import SwiftUI

struct SearchView: View {
    @State private var scrollRect: CGRect = .zero
    @State private var courses = searchCourses
    @State private var tags: [Tag] = []

    var body: some View {
        ZStack {
            Color.background
                .ignoresSafeArea()

            GeometryReader {
                let size = $0.size

                ScrollView(.vertical, showsIndicators: false) {
                    VStack(spacing: 20) {
                        VStack(alignment: .leading) {
                            Text("Search Courses")
                                .customFont(.title2)
                                .padding(.top, 20)
                            
                            TagField(tags: $tags)
                            
                            Text("Separate tags with comma")
                                .customFont(.caption)
                                .padding(.top, 5)
                                .foregroundColor(Color.secondaryText)
                        }

                        ForEach(courses) { course in
                            HCardSection(course: course, isHighlight: true)
                                .elasticScroll(scrollRect: scrollRect, screenSize: size, directions: .all)
                        }
                    }
                    .padding(.horizontal, 26)
                    
                    .offsetExtractor(coordinateSpace: "ScrollView") {
                        scrollRect = $0
                    }
                }
            }
        }
        .foregroundColor(.white)
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
