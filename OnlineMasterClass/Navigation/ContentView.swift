//
//  ContentView.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 24/05/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack {
            Color.background2
                .ignoresSafeArea()
            
            HomeView()
                .environmentObject(Model())
        }
    }
    
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
