//
//  OnlineMasterClassApp.swift
//  OnlineMasterClass
//
//  Created by Lucas Gomes on 24/05/23.
//

import SwiftUI
//var globalSettings = SidebarSettings()
@main
struct OnlineMasterClassApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
